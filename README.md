# Nitec.Go.Demo

## АО НИТ 
## RnD
## 2021Q2

### Тестовое задание

    Run application: 
        docker-compose up -d

    Ports:
        Application: 8080
        Postgress: 5432
        Adminer: 8083

## Swagger screenshot

![alt text](go-demo-screenshot-01.png "Swagger screenshot")