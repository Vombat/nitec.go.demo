## Добро пожаловать, друг!
### Мы очень рады, что ты думаешь о присоединении в нашу команду!

Перед этим мы бы хотели познакомиться с тобой еще ближе и узнать твой технический background. Поэтому мы приготовили для тебя тестовое задание :)

                           ENTRYPOINT

***
###Тестовое задание
Необходимо написать только backend **CRUD API** книжного магазина, который будет уметь добавлять, читать, изменять, удалять информацию о той или иной книге. <br>

* #### Tech requirements:
1. Golang >= 1.14.1
2. Gin/Gorilla Mux
4. Postgre SQL
5. Go modules (go.mod, go.sum)
6. JSON
7. Only native SQL queries

* #### Bonus tech requirements:
1. Docker (10 points)
2. Docker-compose (5 points)
3. Unit tests with unit tags (10 points)
4. Mock tests (10 points)
5. Clean architecture (30 points)

* #### Basic requirements:
1. Swagger
2. Readme.md
3. Logging (дата события; тип события: info, debug, error, warning; сообщение с события)

Таблица о книгах в базе данных должна содержать следующие поля:
| **название поля** | **описание поля** | **тип поля** | | **пример** | 
| -- | ---- | ---- | ---- | ---- |
| id | идентификатор | int | 123456 |
| name | название книги | string | "the others" |
| description | описание книги | "bla bla bla.." |
| price | цена | int | 500 |
| quantity | кол-во | int | 8 |
| created_at | дата создания записи в базу| timestamp | 2020-04-06T05:43:04 |
| updated_at | дата изменения записи в базу| timestamp | 2020-05-09T05:48:04 |
---
> **Роутинг:** <br>
> GET /books - Получить список всех книг и их информацию <br>
> POST /books - Создать книгу <br>
> GET /books/{id} - Получить информацию об определенной книге <br>
> PUT /books/{id} - Обновить информацию об определенной книге <br>
> DELETE /books/{id} - Удалить определенную книгу <br>
----
*  `GET /books` <br>
Входящие параметры: отсутствуют. <br>
Выходящие параметры:
```json
{
    "books": [
        {
            "id": 1,
            "name": "the others",
            "description": "bla bla bla..",
            "price": 200,
            "quantity": 6
        },
        {
            "id": 2,
            "name": "the universe",
            "description": "bla bla bla..",
            "price": 300,
            "quantity": 4
        }
    ]
}
```
---
*  `POST /books` <br>
Входящие параметры:
```json
{
    "name": "the others",
    "description": "bla bla bla..",
    "price": 200,
    "quantity": 10
}
```

Выходящие параметры: <br>
* 200 OK
```json
{
    "message": "Created"
}
```

---
* `GET /books/{id}` <br>
Входящие параметры: отсутствуют. <br>
Выходящие параметры:
```json
{
    "id": 2,
    "name": "the universe",
    "description": "bla bla bla..",
    "price": 300,
    "quantity": 4
}
```
---
* `PUT /books/{id}` <br>
Входящие параметры:
```json
{
    "name": "the others",
    "description": "bla bla bla..",
    "price": 400,
    "quantity": 10
}
```

Выходящие параметры: <br>
* 200 OK
```json
{
    "message": "Updated"
}
```
---
*  `DELETE /books/{id}` <br>
Входящие параметры: id

Выходящие параметры: <br>
* 200 OK
```json
{
    "message": "Deleted"
}
```
---
Common answers:
* 500 InternalError
```json
{
    "message": "InternalError"
}
```

* 400 BadRequest
```json
{
    "message": "BadRequest"
}
```
