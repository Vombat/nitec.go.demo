package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Vombat/nitec.go.demo/main/internal/repository"
	"net/http"
	"strconv"
)

// GetAllBooks godoc
// @Summary Show all books
// @Description Получение списка книг
// @Accept  json
// @Produce  json
// @Success 200 {object} []repository.ResponseBookModel
// @Router /books [get]
func GetAllBooks (c *gin.Context){
	data := repository.GetAllBooks()
	c.JSON(http.StatusOK, gin.H{
		"message": data,
	})
}

// GetBookById godoc
// @Summary Get book by Id
// @Description Получение книги по идентификатору
// @Accept  json
// @Produce  json
// @Param id path integer true "Book id"
// @Success 200 {object} repository.ResponseBookModel
// @Router /books/{id} [get]
func GetBookById (c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("id")); err == nil {
		data := repository.GetBookById(id)
		c.JSON(http.StatusOK, gin.H{
			"message": data,
		})

	} else {
		data := "BadRequest"
		c.JSON(http.StatusBadRequest, gin.H{
			"message": data,
		})
	}
}

// AddBook godoc
// @Summary Create book
// @Description Добавить книгу в БД
// @Accept  json
// @Produce  json
// @Success 200 "Created"
// @Router /books [post]
func AddBook (c *gin.Context) {
	var newBook *repository.CreateUpdateBookModel = &repository.CreateUpdateBookModel{}
	c.BindJSON(newBook)
	data := repository.Create(*newBook)
	c.JSON(http.StatusOK, gin.H{
		"message": data,
	})
}

// UpdateBook godoc
// @Summary Update book
// @Description Изменить книгу в БД
// @Accept  json
// @Produce  json
// @Param id path integer true "Book id"
// @Success 200  "Updated"
// @Router /books/{id} [put]
func UpdateBook (c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("id")); err == nil {
		var updatedBook *repository.CreateUpdateBookModel = &repository.CreateUpdateBookModel{}
		c.BindJSON(updatedBook)
		data := repository.Update(*updatedBook, id)
		c.JSON(http.StatusOK, gin.H{
			"message": data,
		})
	} else {
		data := "BadRequest"
		c.JSON(http.StatusBadRequest, gin.H{
			"message": data,
		})
	}
}

// DeleteBook godoc
// @Summary Delete book
// @Description Удалить книгу по идентификатору
// @Accept  json
// @Produce  json
// @Param id path integer true "Book id"
// @Success 200 "Deleted"
// @Router /books/{id} [delete]
func DeleteBook (c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("id")); err == nil {
		data := repository.Delete(id)
		c.JSON(http.StatusOK, gin.H{
			"message": data,
		})
	} else {
		data := "BadRequest"
		c.JSON(http.StatusBadRequest, gin.H{
			"message": data,
		})
	}
}