package repository

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

type Book struct {
	id          int
	name        string
	description string
	price       int
	quantity    int
	created_at  time.Time
	updated_at  time.Time
}
type ResponseBookModel struct {
	Id          int
	Name        string
	Description string
	Price       int
	Quantity    int
	Created_at  string
	Updated_at  string
}
type CreateUpdateBookModel struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Price       int    `json:"price"`
	Quantity    int    `json:"quantity"`
}

var db *sql.DB

// Production=demo-postgres-api, Development=localhost
var dbUri = "host=localhost port=5432 user=postgres password=postgres dbname=demo_books_db sslmode=disable"
var response = ""

// func parseTime(t string) string {
// 	tt, _ := time.Parse(time.RFC3339, t)
// 	return tt.String()
// }
func Init() {
	var err error
	db, err = sql.Open("postgres", dbUri)
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}
}

func GetAllBooks() string {
	response = ""
	query := "SELECT * FROM books"
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	books := make([]*Book, 0)

	for rows.Next() {
		book := new(Book)
		err := rows.Scan(
			&book.id,
			&book.name,
			&book.description,
			&book.price,
			&book.quantity,
			&book.created_at,
			&book.updated_at,
		)
		if err != nil {
			log.Fatal(err)
		}
		books = append(books, book)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}

	for _, bk := range books {
		responseBook := ResponseBookModel{
			bk.id,
			bk.name,
			bk.description,
			bk.price,
			bk.quantity,
			bk.created_at.String(),
			bk.updated_at.String(),
		}
		response += fmt.Sprintf("%+v", responseBook) + ";"
	}

	return response
}
func GetBookById(id int) string {
	response = ""
	query := "SELECT * FROM books WHERE id = $1"
	row := db.QueryRow(query, id)

	book := new(Book)
	err := row.Scan(
		&book.id,
		&book.name,
		&book.description,
		&book.price,
		&book.quantity,
		&book.created_at,
		&book.updated_at,
	)

	if err == sql.ErrNoRows {
		response = "Not found"
	} else if err != nil {
		response = "BadRequest"
	} else {
		responseBook := ResponseBookModel{
			book.id,
			book.name,
			book.description,
			book.price,
			book.quantity,
			book.created_at.String(),
			book.updated_at.String(),
		}
		response = fmt.Sprintf("%+v", responseBook)
	}

	//tt := book.created_at.String()
	//fmt.Println(tt)

	return response
}
func Create(newBook CreateUpdateBookModel) string {
	created_at := time.Now()
	updated_at := time.Now()
	query := "INSERT INTO books (name, description, price, quantity,created_at,updated_at) VALUES( $1, $2, $3, $4, $5,$6)"
	result, err := db.Exec(query, newBook.Name, newBook.Description, newBook.Price, newBook.Quantity, created_at, updated_at)
	if err != nil {
		log.Fatal(err)
	}

	rowsAffected, err := result.RowsAffected()

	if err != nil {
		response = "Error create"
	}
	response = fmt.Sprintf("Created %d rows", rowsAffected)
	return response
}
func Update(updateBook CreateUpdateBookModel, id int) string {
	query := "UPDATE books SET name = $2, description = $3, price = $4, quantity = $5 , updated_at = $6 WHERE id = $1;"
	result, err := db.Exec(query, id, updateBook.Name, updateBook.Description, updateBook.Price, updateBook.Quantity, time.Now())
	if err != nil {
		log.Fatal(err)
	}
	rowsAffected, err := result.RowsAffected()

	if err != nil {
		response = "Error update"
	}
	if rowsAffected == 1 {
		response = fmt.Sprintf("Updated %d rows", rowsAffected)
	} else {
		response = "Not update"
	}

	return response
}
func Delete(id int) string {
	query := "DELETE FROM books WHERE id=$1"
	result, err := db.Exec(query, id)
	if err != nil {
		log.Fatal(err)
	}
	rowsAffected, err := result.RowsAffected()

	if err != nil {
		response = "Error delete"
	}
	if rowsAffected == 1 {
		response = fmt.Sprintf("Deleted %d rows", rowsAffected)
	} else {
		response = "Not found"
	}

	return response
}
