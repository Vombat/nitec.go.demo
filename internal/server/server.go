package server

import (
	"github.com/dn365/gin-zerolog"
	"github.com/gin-gonic/gin"
	"gitlab.com/Vombat/nitec.go.demo/main/internal/controller"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "gitlab.com/Vombat/nitec.go.demo/main/docs"

)

var Port = ":8080"

// @title Nitec Golang Demo API
// @version 1.0
// @description Goland Demo Server.

// @contact.name API Support
// @contact.url https://gitlab.com/Vombat/nitec.go.demo
// @contact.email abzal.tankibayev@outlook.com

// @host localhost:8080
// @BasePath /api/v1

func Start() {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(ginzerolog.Logger("gin"))

	api := router.Group("/api/v1")
	{
		books := api.Group("/books")
		{
			books.GET("", controller.GetAllBooks)
			books.GET(":id", controller.GetBookById)
			books.POST("", controller.AddBook)
			books.PUT(":id", controller.UpdateBook)
			router.DELETE(":id", controller.DeleteBook)
		}
	}

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	err := router.Run(Port)
	if err != nil {
		return
	}
}
