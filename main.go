package main

import (
	"gitlab.com/Vombat/nitec.go.demo/main/internal/repository"
	"gitlab.com/Vombat/nitec.go.demo/main/internal/server"
)
func main() {
	repository.Init()
	server.Start()
}
